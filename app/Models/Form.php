<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;
use App\Models\Question;


class Form extends Model
{
    use HasFactory;
    protected $connection = "mongodb";

    protected $fillable = ['title', 'description'];

    protected $hidden = ['created_at', 'updated_at'];

    public function questions(){
        return $this->hasMany(Question::class);
    }

}
