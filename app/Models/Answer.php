<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

use App\Models\Question;

class Answer extends Model
{
    use HasFactory;
    protected $connection = "mongodb";

    protected $fillable = ['answer',  'questionId'];

    protected $hidden = ['created_at', 'updated_at'];

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

}
