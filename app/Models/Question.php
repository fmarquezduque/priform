<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

use App\Models\Form;
use App\Models\Answer;

class Question extends Model
{
    use HasFactory;
    
    protected $connection = "mongodb";

    protected $fillable = ['title', 'formId', 'questionType', 'optionName'];

    protected $hidden = ['created_at', 'updated_at'];

    public function forms(){
        return $this->belongsTo(Form::class);
    }

    public function answers(){
        return $this->hasMany(Answer::class);
    }

}
