<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Form;
use Symfony\Component\HttpFoundation\Response;


class FormController extends Controller
{
    /*public function __construct(){
        $this->middleware('auth');
    }*/

    public function index(){
        return response()->json("hello");
    }


    public function store(Request $request, Form $form) {
        
        $validated = $request->validate([
            'title' => 'required|max:125',
            'description' => 'required|max:255',
        ]);
    
        try {

            $form = Form::create($validated);

            return  !$form ?
                    response()
                    ->json([
                        'error' => 'Error al guardar el formulario',
                        'status' =>Response::HTTP_BAD_REQUEST
                     ]) 
                    : 
                     response()
                    ->json([
                        'result' => $form, 
                        'status' => Response::HTTP_CREATED
                    ]);
    
        } catch (\Exception $e) {
            return response()
                    ->json([
                        'error' => 'Error al guardar el formulario', 
                        'status' => Response::HTTP_INTERNAL_SERVER_ERROR
                    ]);
        }
    }

    public function getQuestionsByFormId($formId){

        $form = Form::findOrFail($formId);
        $questions = $form->questions;

        return response()->json([
            'result' => $questions,
            'status' => Response::HTTP_OK,
        ]);
    }


}
 