<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Answer;
use  App\Models\Form;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;


class AnswerController extends Controller
{

    /*public function __construct() {
        $this->middleware('auth');
    }*/

    public function store(Request $request) {

        $answers = $request->input('answers');

        $validator = Validator::make($answers, [
            '*.questionId' => 'required',
            '*.answer' => 'required'
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => 'Formato de respuesta inválido'], Response::HTTP_BAD_REQUEST);
        }
    
        $answerInstances = [];
    
        foreach ($answers as $response) {

            if(Question::findOrFail($response['questionId'])){
               
                $answerInstances[] = new Answer([
                    'question_id' => $response['questionId'],
                    'answer' => $response['answer']
                ]);
            }
        }
    
        try {

            foreach ($answerInstances as $answer) {
                $answer->save();
            }

            return response()->json(['message' => 'Respuestas guardadas correctamente', 'status'=> Response::HTTP_CREATED]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error al guardar las respuestas', 'status'=> Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    } 
}
