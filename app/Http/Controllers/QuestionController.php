<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\Question;
use App\Models\Form;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;


class QuestionController extends Controller
{
    /*public function __construct(){
        $this->middleware('auth');
    }*/
    
    public function store(Request $request, $formId){
        
        $form = Form::findOrFail($formId);
        
        $questions = $request->input('questions');

        $validator = Validator::make($questions, [
            '*.title' => 'required',
            '*.questionType' => [
                'required',
                Rule::in([
                    'text', 
                    'singleChoice', 
                    'multipleChoice'
                ]),
            ],
            '*.optionName' => 'array'
        ]);
    
        if ($validator->fails()) {
            return response()->json(['error' => 'Formato de respuesta inválido'], Response::HTTP_BAD_REQUEST);
        }
    
        $questionInstances = [];
    
        foreach ($questions as $response) {
            $questionInstances[] = new Question([
                'title' => $response['title'],
                'formId' => $form,
                'questionType' => $response['questionType'],
                'optionName' => $response['optionName'],
            ]);
        }
    
        try {

            foreach ($questionInstances as $questions) {
                $questions->save();
            }

            return response()->json(['message' => 'Preguntas guardadas correctamente', 'status'=> Response::HTTP_CREATED]);
        } catch (\Exception $e) {
            return response()->json(['error' => 'Error al guardar las preguntas', 'status'=> Response::HTTP_INTERNAL_SERVER_ERROR]);
        }
    }
}
