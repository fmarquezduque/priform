# PriForm

PriForm es una aplicación web que te permite crear formularios dinamicamente de una forma sencilla y flexible. 

Está app permite la creación de formularios con multiples preguntas y respuestas. Dentro de las opciones de respuestas tenemos opción multiples con unica y multiples respuestas.

## Tech

1. Laravel - REST API
2. ReactJS - FrontEnd
3. MongoDB - Base de datos

## Model

1. Form 
2. Question 
3. QuestionType (Text, SingleChoice, MultipleChoice)
4. Answer
