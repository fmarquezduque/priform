<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FormController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\AnswerController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(FormController::class)->group(function(){
    Route::post('/forms', 'store');
    Route::get('/forms/{formId}/questions', 'getQuestionsByFormId');
});

Route::controller(QuestionController::class)->group(function(){
    Route::post('/forms/{formId}/question', 'store');
});

Route::controller(AnswerController::class)->group(function(){
    Route::post('/forms/answers', 'store');
});